import Orang from "../images/orang.svg";

const Section2 = () => {
  return (
    <>
      <div id="section2" className="container">
        <div className="spacing row">
          <div className="col-lg-6 d-flex justify-content-center">
            <img src={Orang} className="img-fluid person" alt="orang" />
          </div>

          <div className="col-lg-6" style={{ marginTop: "35px" }}>
            <h2>Best Car Rental for any kind of trip in (Lokasimu)!</h2>
            <p>
              Sewa mobil di (Lokasimu) bersama Binar Car Rental jaminan harga
              lebih murah dibandingkan yang lain, kondisi mobil baru, serta
              kualitas pelayanan terbaik untuk perjalanan wisata, bisnis,
              wedding, meeting, dll.
            </p>
            <div className="row">
              <div className="col-lg-12">
                <table className="table-custom">
                  <tr>
                    <td>
                      <svg
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <circle cx="12" cy="12" r="12" fill="#CFD4ED" />
                        <path
                          d="M17.3333 8L9.99996 15.3333L6.66663 12"
                          stroke="#0D28A6"
                          strokeWidth="2"
                          strokeLinecap="round"
                          strokeLinejoin="round"
                        />
                      </svg>
                    </td>
                    <td>Sewa Mobil Dengan Supir di Bali 12 Jam</td>
                  </tr>

                  <tr>
                    <td>
                      <svg
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <circle cx="12" cy="12" r="12" fill="#CFD4ED" />
                        <path
                          d="M17.3333 8L9.99996 15.3333L6.66663 12"
                          stroke="#0D28A6"
                          strokeWidth="2"
                          strokeLinecap="round"
                          strokeLinejoin="round"
                        />
                      </svg>
                    </td>
                    <td>Sewa Mobil Lepas Kunci di Bali 24 Jam</td>
                  </tr>
                  <tr>
                    <td>
                      <svg
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <circle cx="12" cy="12" r="12" fill="#CFD4ED" />
                        <path
                          d="M17.3333 8L9.99996 15.3333L6.66663 12"
                          stroke="#0D28A6"
                          strokeWidth="2"
                          strokeLinecap="round"
                          strokeLinejoin="round"
                        />
                      </svg>
                    </td>
                    <td>Sewa Mobil Jangka Panjang Bulanan</td>
                  </tr>

                  <tr>
                    <td>
                      <svg
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <circle cx="12" cy="12" r="12" fill="#CFD4ED" />
                        <path
                          d="M17.3333 8L9.99996 15.3333L6.66663 12"
                          stroke="#0D28A6"
                          strokeWidth="2"
                          strokeLinecap="round"
                          strokeLinejoin="round"
                        />
                      </svg>
                    </td>
                    <td>Gratis Antar - Jemput Mobil di Bandara</td>
                  </tr>

                  <tr>
                    <td>
                      <svg
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <circle cx="12" cy="12" r="12" fill="#CFD4ED" />
                        <path
                          d="M17.3333 8L9.99996 15.3333L6.66663 12"
                          stroke="#0D28A6"
                          strokeWidth="2"
                          strokeLinecap="round"
                          strokeLinejoin="round"
                        />
                      </svg>
                    </td>
                    <td>Layanan Airport Transfer / Drop In Out</td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Section2;
