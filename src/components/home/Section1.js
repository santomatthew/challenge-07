import { useEffect, useState } from "react";
import { Link, useLocation } from "react-router-dom";
// import Mobil from "../../../public/images/mobil.svg";
const Section1 = () => {
  const location = useLocation();
  const [showButton, setShowButton] = useState();

  useEffect(() => {
    if (location.pathname === "/cars") {
      setShowButton(false);
    } else {
      setShowButton(true);
    }
  }, [location.pathname]);

  return (
    <>
      <div
        className="bground"
        style={{ backgroundColor: "#F1F3FF", overflow: "hidden" }}
      >
        <div className="container">
          <div id="section1" className="row">
            <div className="col-lg-6" style={{ marginTop: "10px" }}>
              <h1 className="textjudul">
                <b>Sewa & Rental Mobil terbaik di kawasan (Lokasimu)</b>
              </h1>
              <p>
                Selamat datang di Binar Car Rental. Kami menyediakan mobil
                kualitas terbaik dengan harga terjangkau. Selalu siap melayani
                kebutuhanmu untuk sewa mobil selama 24 jam.
              </p>
              {showButton ? (
                <Link
                  to="/cars"
                  type="button"
                  className="btn btn-success showbutton"
                >
                  Mulai Sewa Mobil
                </Link>
              ) : (
                <p></p>
              )}
            </div>
            <div className="col-lg-6 d-flex justify-content-center">
              <img
                src="/images/mobil.svg"
                className="img-fluid car"
                style={{ marginTop: "80px" }}
                alt="mobil"
              />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Section1;
