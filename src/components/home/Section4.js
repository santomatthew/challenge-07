const Section4 = () => {
  return (
    <>
      <div className="container">
        <div className="spacing row">
          <div className="col-lg-12  d-flex justify-content-center">
            <h3>Testimonial</h3>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-12  d-flex justify-content-center">
            <p>Berbagai review positif dari para pelanggan kami</p>
          </div>
        </div>
      </div>
    </>
  );
};

export default Section4;
