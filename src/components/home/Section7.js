const Section7 = () => {
  return (
    <>
      <div id="section4" className="container">
        <div className="spacing row">
          <div className="col-lg-6">
            <h3>Frequently Asked Question</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
          </div>

          <div className="col-lg-6">
            <div className="row" style={{ position: "static" }}>
              <div className="col-lg-12">
                <div className="accordion-item accordion-spacing">
                  <h2 className="accordion-header" id="flush-heading1">
                    <button
                      className="accordion-button collapsed"
                      type="button"
                      data-bs-toggle="collapse"
                      data-bs-target="#flush-collapse1"
                      aria-expanded="false"
                      aria-controls="flush-collapse1"
                    >
                      Apa saja syarat yang dibutuhkan?
                    </button>
                  </h2>
                  <div
                    id="flush-collapse1"
                    className="accordion-collapse collapse"
                    aria-labelledby="flush-heading1"
                    data-bs-parent="#accordionFlushExample"
                  >
                    <div className="accordion-body">
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                      Neque, recusandae iste accusamus nobis quos esse, est modi
                      cupiditate aperiam illum voluptatibus amet quae quis
                      sapiente. Reiciendis inventore esse facere sint.
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="row">
              <div className="col-lg-12">
                <div className="accordion-item accordion-spacing">
                  <h2 className="accordion-header" id="flush-heading2">
                    <button
                      className="accordion-button collapsed"
                      type="button"
                      data-bs-toggle="collapse"
                      data-bs-target="#flush-collapse2"
                      aria-expanded="false"
                      aria-controls="flush-collapse2"
                    >
                      Berapa hari minimal sewa mobil lepas kunci?
                    </button>
                  </h2>
                  <div
                    id="flush-collapse2"
                    className="accordion-collapse collapse"
                    aria-labelledby="flush-heading2"
                    data-bs-parent="#accordionFlushExample"
                  >
                    <div className="accordion-body">
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                      Neque, recusandae iste accusamus nobis quos esse, est modi
                      cupiditate aperiam illum voluptatibus amet quae quis
                      sapiente. Reiciendis inventore esse facere sint.
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="row">
              <div className="col-lg-12">
                <div className="accordion-item accordion-spacing">
                  <h2 className="accordion-header" id="flush-heading4">
                    <button
                      className="accordion-button collapsed"
                      type="button"
                      data-bs-toggle="collapse"
                      data-bs-target="#flush-collapse3"
                      aria-expanded="false"
                      aria-controls="flush-collapse3"
                    >
                      Berapa hari sebelumnya sebaiknya booking sewa mobil?
                    </button>
                  </h2>
                  <div
                    id="flush-collapse3"
                    className="accordion-collapse collapse"
                    aria-labelledby="flush-heading3"
                    data-bs-parent="#accordionFlushExample"
                  >
                    <div className="accordion-body">
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                      Neque, recusandae iste accusamus nobis quos esse, est modi
                      cupiditate aperiam illum voluptatibus amet quae quis
                      sapiente. Reiciendis inventore esse facere sint.
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="row">
              <div className="col-lg-12">
                <div className="accordion-item accordion-spacing">
                  <h2 className="accordion-header" id="flush-heading4">
                    <button
                      className="accordion-button collapsed"
                      type="button"
                      data-bs-toggle="collapse"
                      data-bs-target="#flush-collapse4"
                      aria-expanded="false"
                      aria-controls="flush-collapse5"
                    >
                      Apakah Ada biaya antar-jemput?
                    </button>
                  </h2>
                  <div
                    id="flush-collapse4"
                    className="accordion-collapse collapse"
                    aria-labelledby="flush-heading4"
                    data-bs-parent="#accordionFlushExample"
                  >
                    <div className="accordion-body">
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                      Neque, recusandae iste accusamus nobis quos esse, est modi
                      cupiditate aperiam illum voluptatibus amet quae quis
                      sapiente. Reiciendis inventore esse facere sint.
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="row">
              <div className="col-lg-12">
                <div className="accordion-item accordion-spacing">
                  <h2 className="accordion-header" id="flush-heading5">
                    <button
                      className="accordion-button collapsed"
                      type="button"
                      data-bs-toggle="collapse"
                      data-bs-target="#flush-collapse5"
                      aria-expanded="false"
                      aria-controls="flush-collapse5"
                    >
                      Bagaimana jika terjadi kecelakaan
                    </button>
                  </h2>
                  <div
                    id="flush-collapse5"
                    className="accordion-collapse collapse"
                    aria-labelledby="flush-heading5"
                    data-bs-parent="#accordionFlushExample"
                  >
                    <div className="accordion-body">
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                      Neque, recusandae iste accusamus nobis quos esse, est modi
                      cupiditate aperiam illum voluptatibus amet quae quis
                      sapiente. Reiciendis inventore esse facere sint.
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Section7;
