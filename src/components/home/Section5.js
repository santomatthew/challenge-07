import { useEffect } from "react";

import Person1 from "../images/person1.svg";
import Person2 from "../images/person2.svg";

const Section5 = () => {
  useEffect(() => {
    if (window.loadOwlCarousel) {
      window.loadOwlCarousel();
    }
  }, []);
  return (
    <>
      <div id="owl-container" style={{ marginTop: "-5px" }}>
        <div className="owl-carousel owl-theme">
          {/* <!-- Testi 1 --> */}
          <div className="item">
            <div
              className="card"
              style={{ padding: "30px", backgroundColor: "#F1F3FF" }}
            >
              <div className="card-body">
                <div className="row">
                  <div className="col-lg-4 d-flex justify-content-center">
                    <img
                      src={Person1}
                      className="img-fluid"
                      alt="person1"
                      style={{ width: "150px", height: "150px" }}
                    />
                  </div>
                  <div className="col-lg-8">
                    <div className="star row">
                      <div className="col-lg-12 ">
                        <svg
                          width="80"
                          height="16"
                          viewBox="0 0 80 16"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            d="M8 0L9.79611 5.52786H15.6085L10.9062 8.94427L12.7023 14.4721L8 11.0557L3.29772 14.4721L5.09383 8.94427L0.391548 5.52786H6.20389L8 0Z"
                            fill="#F9CC00"
                          />
                          <path
                            d="M24 0L25.7961 5.52786H31.6085L26.9062 8.94427L28.7023 14.4721L24 11.0557L19.2977 14.4721L21.0938 8.94427L16.3915 5.52786H22.2039L24 0Z"
                            fill="#F9CC00"
                          />
                          <path
                            d="M40 0L41.7961 5.52786H47.6085L42.9062 8.94427L44.7023 14.4721L40 11.0557L35.2977 14.4721L37.0938 8.94427L32.3915 5.52786H38.2039L40 0Z"
                            fill="#F9CC00"
                          />
                          <path
                            d="M56 0L57.7961 5.52786H63.6085L58.9062 8.94427L60.7023 14.4721L56 11.0557L51.2977 14.4721L53.0938 8.94427L48.3915 5.52786H54.2039L56 0Z"
                            fill="#F9CC00"
                          />
                          <path
                            d="M72 0L73.7961 5.52786H79.6085L74.9062 8.94427L76.7023 14.4721L72 11.0557L67.2977 14.4721L69.0938 8.94427L64.3915 5.52786H70.2039L72 0Z"
                            fill="#F9CC00"
                          />
                        </svg>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-12">
                        Lorem1 ipsum dolor sit amet consectetur adipisicing
                        elit. Unde at, odit illum obcaecati repudiandae fugit
                        quisquam maxime sit perspiciatis a, quia temporibus,
                        ipsam illo ipsum et voluptatum! Similique, quasi
                        deserunt.
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-12">
                        <b style={{ marginBottom: "100px" }}>
                          John Dee 32, Bromo
                        </b>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* <!-- Testi 2 --> */}
          <div className="item">
            <div
              className="card"
              style={{ padding: "30px", backgroundColor: "#F1F3FF" }}
            >
              <div className="card-body">
                <div className="row">
                  <div className="col-lg-4 d-flex justify-content-center">
                    <img
                      src={Person2}
                      className="img-fluid"
                      alt="person1"
                      style={{ width: "150px", height: "150px" }}
                    />
                  </div>
                  <div className="col-lg-8">
                    <div className="star row">
                      <div className="col-lg-12 ">
                        <svg
                          width="80"
                          height="16"
                          viewBox="0 0 80 16"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            d="M8 0L9.79611 5.52786H15.6085L10.9062 8.94427L12.7023 14.4721L8 11.0557L3.29772 14.4721L5.09383 8.94427L0.391548 5.52786H6.20389L8 0Z"
                            fill="#F9CC00"
                          />
                          <path
                            d="M24 0L25.7961 5.52786H31.6085L26.9062 8.94427L28.7023 14.4721L24 11.0557L19.2977 14.4721L21.0938 8.94427L16.3915 5.52786H22.2039L24 0Z"
                            fill="#F9CC00"
                          />
                          <path
                            d="M40 0L41.7961 5.52786H47.6085L42.9062 8.94427L44.7023 14.4721L40 11.0557L35.2977 14.4721L37.0938 8.94427L32.3915 5.52786H38.2039L40 0Z"
                            fill="#F9CC00"
                          />
                          <path
                            d="M56 0L57.7961 5.52786H63.6085L58.9062 8.94427L60.7023 14.4721L56 11.0557L51.2977 14.4721L53.0938 8.94427L48.3915 5.52786H54.2039L56 0Z"
                            fill="#F9CC00"
                          />
                          <path
                            d="M72 0L73.7961 5.52786H79.6085L74.9062 8.94427L76.7023 14.4721L72 11.0557L67.2977 14.4721L69.0938 8.94427L64.3915 5.52786H70.2039L72 0Z"
                            fill="#F9CC00"
                          />
                        </svg>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-12">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        Unde at, odit illum obcaecati repudiandae fugit quisquam
                        maxime sit perspiciatis a, quia temporibus, ipsam illo
                        ipsum et voluptatum! Similique, quasi deserunt.
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-12">
                        <b>John Dee 32, Bromo</b>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="item">
            <div
              className="card"
              style={{ padding: "30px", backgroundColor: "#F1F3FF" }}
            >
              <div className="card-body">
                <div className="row">
                  <div className="col-lg-4 d-flex justify-content-center">
                    <img
                      src={Person1}
                      className="img-fluid"
                      alt="person1"
                      style={{ width: "150px", height: "150px" }}
                    />
                  </div>
                  <div className="col-lg-8">
                    <div className="star row">
                      <div className="col-lg-12 ">
                        <svg
                          width="80"
                          height="16"
                          viewBox="0 0 80 16"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            d="M8 0L9.79611 5.52786H15.6085L10.9062 8.94427L12.7023 14.4721L8 11.0557L3.29772 14.4721L5.09383 8.94427L0.391548 5.52786H6.20389L8 0Z"
                            fill="#F9CC00"
                          />
                          <path
                            d="M24 0L25.7961 5.52786H31.6085L26.9062 8.94427L28.7023 14.4721L24 11.0557L19.2977 14.4721L21.0938 8.94427L16.3915 5.52786H22.2039L24 0Z"
                            fill="#F9CC00"
                          />
                          <path
                            d="M40 0L41.7961 5.52786H47.6085L42.9062 8.94427L44.7023 14.4721L40 11.0557L35.2977 14.4721L37.0938 8.94427L32.3915 5.52786H38.2039L40 0Z"
                            fill="#F9CC00"
                          />
                          <path
                            d="M56 0L57.7961 5.52786H63.6085L58.9062 8.94427L60.7023 14.4721L56 11.0557L51.2977 14.4721L53.0938 8.94427L48.3915 5.52786H54.2039L56 0Z"
                            fill="#F9CC00"
                          />
                          <path
                            d="M72 0L73.7961 5.52786H79.6085L74.9062 8.94427L76.7023 14.4721L72 11.0557L67.2977 14.4721L69.0938 8.94427L64.3915 5.52786H70.2039L72 0Z"
                            fill="#F9CC00"
                          />
                        </svg>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-12">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        Unde at, odit illum obcaecati repudiandae fugit quisquam
                        maxime sit perspiciatis a, quia temporibus, ipsam illo
                        ipsum et voluptatum! Similique, quasi deserunt.
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-12">
                        <b>John Dee 32, Bromo</b>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* <!-- Testi 2 --> */}
          <div className="item">
            <div
              className="card"
              style={{ padding: "30px", backgroundColor: "#F1F3FF" }}
            >
              <div className="card-body">
                <div className="row">
                  <div className="col-lg-4 d-flex justify-content-center">
                    <img
                      src={Person2}
                      className="img-fluid"
                      alt="person1"
                      style={{ width: "150px", height: "150px" }}
                    />
                  </div>
                  <div className="col-lg-8">
                    <div className="star row">
                      <div className="col-lg-12 ">
                        <svg
                          width="80"
                          height="16"
                          viewBox="0 0 80 16"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            d="M8 0L9.79611 5.52786H15.6085L10.9062 8.94427L12.7023 14.4721L8 11.0557L3.29772 14.4721L5.09383 8.94427L0.391548 5.52786H6.20389L8 0Z"
                            fill="#F9CC00"
                          />
                          <path
                            d="M24 0L25.7961 5.52786H31.6085L26.9062 8.94427L28.7023 14.4721L24 11.0557L19.2977 14.4721L21.0938 8.94427L16.3915 5.52786H22.2039L24 0Z"
                            fill="#F9CC00"
                          />
                          <path
                            d="M40 0L41.7961 5.52786H47.6085L42.9062 8.94427L44.7023 14.4721L40 11.0557L35.2977 14.4721L37.0938 8.94427L32.3915 5.52786H38.2039L40 0Z"
                            fill="#F9CC00"
                          />
                          <path
                            d="M56 0L57.7961 5.52786H63.6085L58.9062 8.94427L60.7023 14.4721L56 11.0557L51.2977 14.4721L53.0938 8.94427L48.3915 5.52786H54.2039L56 0Z"
                            fill="#F9CC00"
                          />
                          <path
                            d="M72 0L73.7961 5.52786H79.6085L74.9062 8.94427L76.7023 14.4721L72 11.0557L67.2977 14.4721L69.0938 8.94427L64.3915 5.52786H70.2039L72 0Z"
                            fill="#F9CC00"
                          />
                        </svg>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-12">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        Unde at, odit illum obcaecati repudiandae fugit quisquam
                        maxime sit perspiciatis a, quia temporibus, ipsam illo
                        ipsum et voluptatum! Similique, quasi deserunt.
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-12">
                        <b>John Dee 32, Bromo</b>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default Section5;
