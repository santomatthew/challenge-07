import { Link } from "react-router-dom";
const Section6 = () => {
  return (
    <>
      <div className="container">
        <div
          className="row d-lg-flex justify-content-center sewamobil"
          style={{ marginTop: "50px", backgroundColor: "#0D28A6" }}
        >
          <div className="col-lg-5" style={{ textAlign: "center" }}>
            <h2>Sewa Mobil di (Lokasimu) Sekarang</h2>
            <br />
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua.{" "}
            </p>
            <br />
            <Link to="/cars" type="button" className="btn btn-success">
              Mulai Sewa Mobil
            </Link>
          </div>
        </div>
      </div>
    </>
  );
};

export default Section6;
