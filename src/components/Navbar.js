import { Link } from "react-router-dom";

const Navbar = () => {
  return (
    <>
      <div style={{ backgroundColor: "#F1F3FF" }}>
        <div className="container">
          <nav className="navbar navbar-expand-lg navbar-light navbarstyling">
            <div className="container-lg">
              <Link
                className="navbar-link"
                data-bs-container="body"
                data-bs-toggle="popover"
                data-bs-placement="top"
                data-bs-content="Top popover"
                to="/"
              >
                <svg
                  width="100"
                  height="34"
                  viewBox="0 0 100 34"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <rect width="100" height="34" fill="#0D28A6" />
                </svg>
              </Link>
              <button
                className="navbar-toggler"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent"
                aria-controls="navbarText"
                aria-expanded="false"
                aria-label="Toggle navigation"
              >
                <span className="navbar-toggler-icon"></span>
              </button>
              <div
                className="collapse navbar-collapse "
                id="navbarSupportedContent"
              >
                <ul className="navbar-nav ms-auto mb-lg-0">
                  <li className="d-lg-none">
                    <a className="navbar-nav-toggler" href="#">
                      X
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link active me-2" href="#section1">
                      Our Service
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link active me-2" href="#section2">
                      Why Us
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link active me-2" href="#section3">
                      Testimonial
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link active me-2" href="#section4">
                      FAQ
                    </a>
                  </li>
                  <li className="nav-item">
                    <a
                      className="navbar-link btn btn-success link-light"
                      href="#"
                    >
                      Register
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </nav>
        </div>
      </div>
    </>
  );
};

export default Navbar;
