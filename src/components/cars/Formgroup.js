import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { setFilter } from "../../reducers/api-store";

const Formgroup = () => {
  const dispatch = useDispatch();

  const [driver, setDriver] = useState();
  const [tanggal, setTanggal] = useState();
  const [jemput, setJemput] = useState();
  const [penumpang, setPenumpang] = useState();

  const [data, setData] = useState({});

  useEffect(() => {
    dispatch(setFilter(data));
  }, [dispatch, data]);

  const handleSubmit = (e) => {
    e.preventDefault();
    setData({
      driver: driver,
      tanggal: tanggal,
      jemput: jemput,
      penumpang: parseInt(penumpang),
    });
  };

  return (
    <>
      <div className="row d-flex justify-content-center">
        <div className="col-lg-10" style={{ textAlign: "center" }}>
          <div
            className="card formsetting"
            style={{
              boxShadow: "0px 0px 10px",
              position: "relative",
              bottom: "70%",
            }}
          >
            <form className="needs-validation" noValidate>
              <div className="card-body">
                <div className="row">
                  <div className="col-lg-10" style={{ textAlign: "left" }}>
                    <div className="row">
                      <div className="col-lg-3">
                        <div className="row">
                          <div className="col-lg-12">
                            <p>Tipe Driver</p>
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-lg-12">
                            <select
                              className="form-select"
                              aria-label="Default select example"
                              placeholder="Pilih Driver"
                              onChange={(e) => setDriver(e.target.value)}
                              required
                            >
                              <option hidden>Pilih Driver</option>
                              <option value="1">Dengan Sopir</option>
                              <option value="2">Tanpa Sopir</option>
                            </select>
                            <div className="invalid-feedback">Pilih driver</div>
                          </div>
                        </div>
                      </div>
                      <div className="col-lg-3">
                        <div className="row">
                          <div className="col-lg-12">
                            <p>Tanggal</p>
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-lg-12">
                            <input
                              type="date"
                              className="form-control"
                              id="tanggal"
                              onChange={(e) => setTanggal(e.target.value)}
                              required
                            />
                            <div className="invalid-feedback">
                              Pilih tanggal
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="col-lg-3">
                        <div className="row">
                          <div className="col-lg-12">
                            <p>Waktu Jemput/Ambil</p>
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-lg-12">
                            <input
                              type="time"
                              id="waktu"
                              className="form-control"
                              placeholder="00:00"
                              onChange={(e) => setJemput(e.target.value)}
                              required
                            />
                            <div className="invalid-feedback">
                              Pilih tanggal pengambilan
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="col-lg-3">
                        <div className="row">
                          <div className="col-lg-12">
                            <p>Jumlah Penumpang</p>
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-lg-12">
                            <input
                              id="penumpang"
                              type="number"
                              className="form-control"
                              placeholder="Jumlah Penumpang"
                              onChange={(e) => setPenumpang(e.target.value)}
                              required
                            />
                            <div className="invalid-feedback">
                              Isi jumlah Penumpang
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-2">
                    <div className="row">
                      <div className="col-lg-12">
                        <button
                          className="btn btn-success btnsetting "
                          style={{ width: "100%" }}
                          onClick={handleSubmit}
                        >
                          Cari Mobil
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </>
  );
};
export default Formgroup;
