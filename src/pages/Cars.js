import Formgroup from "../components/cars/Formgroup";
import Listcars from "../components/cars/Listcars";
import Section1 from "../components/home/Section1";

const Cars = () => {
  return (
    <>
      <Section1 />
      <Formgroup />
      <Listcars />
    </>
  );
};

export default Cars;
